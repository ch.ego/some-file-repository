package ru.chelnokov.study.lab2;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class XMLParseTask {
    public static void main(String[] args) throws IOException {
        XmlMapper xmlMapper = new XmlMapper();
        JsonNode node = xmlMapper.readTree(new File("pom.xml"));
        ObjectMapper jsonMapper = new ObjectMapper();
        String json = jsonMapper.writeValueAsString(node);

        try (BufferedWriter writer = new BufferedWriter(new FileWriter("src/main/java/ru/chelnokov/study/lab2/json/result.json"))) {
            writer.write(json);
        }
    }
}
