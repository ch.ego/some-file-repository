package ru.chelnokov.study.lab2;

import org.json.JSONObject;
import org.json.XML;

import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class XMLParseTaskII {
    public static void main(String[] args) throws IOException {
        JSONObject xmlJSONObj = XML.toJSONObject(new FileReader("pom.xml"));
        String jsonPrettyPrintString = xmlJSONObj.toString(4);

        try (BufferedWriter writer = new BufferedWriter(new FileWriter("src/main/java/ru/chelnokov/study/lab2/json/resultII.json"))) {
            writer.write(jsonPrettyPrintString);
        }
    }
}
