package ru.chelnokov.study.lab4;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.nio.file.Files;
import java.util.Optional;

public class Copyist {
    private static final Logger log = LogManager.getLogger(Copyist.class);

    /**
     * метод, демонстрирующий работу других методов)
     */
    public static void main(String[] args) {
        File source1 = new File("src/main/resources/XMLParseTask.java");
        File target1 = new File(source1.getParent() + "\\copyOf" + source1.getName());

        File source2 = new File("src/main/resources/XMLParseTaskII.java");
        File target2 = new File(source2.getParent() + "\\copyOf" + source2.getName());

        copyNio(source1, target1);
        copyNio(source2, target2);

        copyWithNoNio(source1, target1);
        copyWithNoNio(source2, target2);

        new Thread(() -> copyNio(source1, target1)).start();
        new Thread(() -> copyNio(source2, target2)).start();

        new Thread(() -> copyWithNoNio(source1, target1)).start();
        new Thread(() -> copyWithNoNio(source2, target2)).start();
    }

    /**
     * копирует файл с помощью построчного считывания
     *
     * @param source адрес исходного файла
     * @param target адрес скопированного файла
     */
    public static void copyWithNoNio(File source, File target) {
        renameUnique(target);
        try (BufferedReader reader = new BufferedReader(new FileReader(source));
             BufferedWriter writer = new BufferedWriter(new FileWriter(target))) {
            String currentStr;
            while ((currentStr = reader.readLine()) != null) {
                writer.write(currentStr + "\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * копирует с помощью java.nio.file.Files
     *
     * @param source адрес исходного файла
     * @param target адрес конечного файла
     */
    public static void copyNio(File source, File target) {
        try {
            renameUnique(target);
            Files.copy(source.toPath(), target.toPath());
            System.out.println("File " + source.getName() + " has been copied!");
        } catch (IOException e) {
            log.error("#copy " + source.getName());
        }
    }

    /**
     * переименовывает конечный файл, если такой уже существует
     *
     * @param target конечный файл
     */
    private static void renameUnique(File target) {
        int counter = 1;
        while (target.exists()) {
            target.renameTo(new File(target.getParent() + "\\" +
                    target.getName().substring(0, target.getName().lastIndexOf(".")) +
                    "(" + counter + ")." + getExtension(target.getName())));
            counter++;
        }
    }

    public static String getExtension(String filename) {
        return Optional.ofNullable(filename)
                .filter(f -> f.contains("."))
                .map(f -> f.substring(filename.lastIndexOf(".") + 1)).orElse("");
    }
}
