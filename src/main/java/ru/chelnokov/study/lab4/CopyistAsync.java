package ru.chelnokov.study.lab4;

import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.AsynchronousFileChannel;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import static java.nio.file.StandardOpenOption.*;

public class CopyistAsync {
    public static void main(String[] args) throws IOException, ExecutionException, InterruptedException {
        File source1 = new File("src/main/resources/XMLParseTask.java");
        File target1 = new File(source1.getParent() + "\\copyOf" + source1.getName());

        File source2 = new File("src/main/resources/XMLParseTaskII.java");
        File target2 = new File(source2.getParent() + "\\copyOf" + source2.getName());

        writeFile(source1, target1);
        writeFile(source2, target2);
    }

    public static String readContent(Path file) throws ExecutionException, InterruptedException, IOException {
        AsynchronousFileChannel fileChannel = AsynchronousFileChannel.open(
                file, StandardOpenOption.READ);

        ByteBuffer buffer = ByteBuffer.allocate(1024);

        Future<Integer> operation = fileChannel.read(buffer, 0);

        operation.get();

        String fileContent = new String(buffer.array()).trim();
        buffer.clear();
        return fileContent;
    }

    public static void writeFile(File source, File target) throws IOException, ExecutionException, InterruptedException {
        Path path = target.toPath();
        AsynchronousFileChannel fileChannel = AsynchronousFileChannel.open(
                path, WRITE, CREATE);

        ByteBuffer buffer = ByteBuffer.allocate(1024);

        String content = readContent(source.toPath());
        buffer.put(content.getBytes());
        buffer.flip();

        Future<Integer> operation = fileChannel.write(buffer, 0);
        buffer.clear();

        operation.get();
    }
}
