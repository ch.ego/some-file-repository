package ru.chelnokov.study.lab3;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class FileComparator {

    public static void main(String[] args) throws IOException {
        File file1 = new File("src/main/java/ru/chelnokov/study/lab2/XMLParseTask.java");
        File file2 = new File("src/main/java/ru/chelnokov/study/lab2/XMLParseTaskII.java");

        compare(file1, file2);
    }

    static void compare(File file1, File file2) throws IOException {
        List<String> list1 = Files.lines(file1.toPath()).collect(Collectors.toList());
        List<String> list2 = Files.lines(file2.toPath()).collect(Collectors.toList());

        System.out.println("Content from " + file1.getName() + " which is not there in " + file2.getName());
        List<String> tmpList = new ArrayList<>(list1);
        tmpList.removeAll(list2);
        tmpList.forEach(System.out::println);
        int value1 = tmpList.size();

        System.out.println("Content from " + file2.getName() + " which is not there in " + file1.getName());
        tmpList = list2;
        tmpList.removeAll(list1);
        tmpList.forEach(System.out::println);
        int value2 = tmpList.size();

        System.out.println("Approximate line difference value: " + Math.max(value1, value2));
    }
}
