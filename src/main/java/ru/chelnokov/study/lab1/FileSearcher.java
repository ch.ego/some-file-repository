package ru.chelnokov.study.lab1;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;

public class FileSearcher {
    private static final Logger log = LogManager.getLogger(FileSearcher.class);

    public static void main(String[] args) {
        Scanner reader = new Scanner(System.in);
        String username = System.getProperty("user.name");

        //допустим, ищет файлы с чьего-нибудь рабочего стола
        File currentDir = new File("C:\\Users\\" + username + "\\Desktop");

        String request = reader.nextLine();
        System.out.println("FOUND FILES:\n" +
                scanDir(currentDir.getAbsolutePath(), request)
        );
    }

    static List<Path> scanDir(String path, String string) {
        var result = new LinkedList<Path>();
        File target = new File(path);
        if (target.isFile()) {
            try {
                if (containsFilename(target, string)) {
                    result.add(target.toPath());
                } else if (containsContent(target.toPath(), string)) {
                    result.add(target.toPath());
                }
            } catch (IOException | NullPointerException e) {
                log.warn("#scanDir() " + e.getMessage() + " " + target.getName());
            }
        } else if (target.isDirectory()) {
            for (File file : Objects.requireNonNull(target.listFiles())) {
                result.addAll(scanDir(file.getAbsolutePath(), string));
            }
        }
        return result;
    }

    private static boolean containsFilename(File file, String filename) {
        return file.getName().toLowerCase().contains(filename.toLowerCase());
    }

    private static boolean containsContent(Path path, String content) throws IOException {
        return Files.probeContentType(path).contains("text/plain") &&
                Files.lines(path, StandardCharsets.UTF_8).anyMatch(s -> s.contains(content));
    }
}
